﻿using System;
using System.Windows.Forms;

namespace CivXmlEditor
{
    public partial class SearchForm : Form
    {
        private CivXmlEditor BaseForm { get; set; }

        public SearchForm(CivXmlEditor form, string[] tags)
        {
            BaseForm = form;
            InitializeComponent();
            ResultListBox.Items.Clear();
            ResultListBox.Items.AddRange(tags);
        }

        private void ResultListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (ResultListBox.SelectedIndex != -1 && ResultListBox.SelectedItem != null)
            {
                BaseForm.SearchTag(ResultListBox.SelectedItem.ToString(), true);
            }
        }
    }
}
