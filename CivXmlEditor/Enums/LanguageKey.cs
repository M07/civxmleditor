﻿
namespace CivXmlEditor.Enums
{
    public enum AllowedLanguageKey
    {
        IsFrenchAllowed,
        IsGermanAllowed,
        IsItalianAllowed,
        IsSpanishAllowed
    }
}
