﻿using CivXmlEditor.Managers;
using System;
using System.Windows.Forms;
using System.Diagnostics;
using System.Drawing;
using System.Xml.Linq;
using System.Linq;
using CivXmlEditor.Enums;

namespace CivXmlEditor
{
    public partial class CivXmlEditor : Form
    {
        public FileManager FileManager { get; set; }
        TagManager tagManager;
        bool InsideSearch { get; set; }
        bool[] MaleOptions { get; set; }
        bool[] FemaleOptions { get; set; }
        bool[] SingularOptions { get; set; }
        bool[] PluralOptions { get; set; }

        public TextBox CurrentLanguageTextBox { get; private set; }

        public CivXmlEditor()
        {
            InitializeComponent();
            // Properties.Settings.Default.Reset(); // Allow to reset User saves
            FileManager = new FileManager();
            tagManager = new TagManager();
            InsideSearch = false;
            MaleOptions = new bool[5];
            FemaleOptions = new bool[5];
            SingularOptions = new bool[5];
            PluralOptions = new bool[5];
        }

        public void UpdateFilesListBox()
        {
            string[] filesName = FileManager.GetLoadedXmlFilesFromFolder();
            if (filesName.Length > 0)
            {
                UpdateLogText(String.Format("{0} XML file(s) found in this folder", filesName.Length), TraceLevel.Info);
                FilesListBox.Items.Clear();
                FilesListBox.Items.AddRange(filesName);
                tagManager.InitXmlElements(FileManager.GetXmlElements());
                FilesListBox.SetSelected(0, true);
                LanguageTextBox_GotFocus(EnglishTextBox, 0);
            }
            else
            {
                var form = new FirstTimeForm(this);
                form.Show();
            }
        }

        private void FilesListBox_SelectedValueChanged(object sender, EventArgs e)
        {
            if (FilesListBox.SelectedIndex != -1 && FilesListBox.SelectedItem != null)
            {
                if(!InsideSearch)
                {
                    tagManager.UpdateCurrentTag(FilesListBox.SelectedItem.ToString());
                    UpdateCurrentTag();
                }
            }
        }

        private void UpdateCurrentTag()
        {
            XElement currentTag = tagManager.CurrentTag;
            if (currentTag == null)
            {
                TagTextBox.Text = string.Empty;
                EnglishTextBox.Text = string.Empty;
                FrenchTextBox.Text = string.Empty;
                GermanTextBox.Text = string.Empty;
                ItalianTextBox.Text = string.Empty;
                SpanishTextBox.Text = string.Empty;
                TagIndexTextBox.Text = string.Empty;
                for(int i = 0; i < 5; ++i)
                {
                    MaleOptions[i] = false;
                    FemaleOptions[i] = false;
                    SingularOptions[i] = false;
                    PluralOptions[i] = false;
                }
            }
            else
            {
                string tag = TagManager.GetTextValue(currentTag.Element("Tag"));
                TagTextBox.Text = tag;
                var element = currentTag.Element("English");
                EnglishTextBox.Text = TagManager.GetTextValue(element);
                UpdateLangOption(0, TagManager.GetTextGender(element), TagManager.GetTextPlural(element));

                element = currentTag.Element("French");
                FrenchTextBox.Text = TagManager.GetTextValue(element);
                UpdateLangOption(1, TagManager.GetTextGender(element), TagManager.GetTextPlural(element));
                if (IsLanguageAllowed(AllowedLanguageKey.IsFrenchAllowed) || EnglishTextBox.Text != FrenchTextBox.Text)
                {
                    EnableLanguage(AllowedLanguageKey.IsFrenchAllowed);
                }
                else
                {
                    DisableLanguage(AllowedLanguageKey.IsFrenchAllowed, 1);
                }

                element = currentTag.Element("German");
                GermanTextBox.Text = TagManager.GetTextValue(element);
                UpdateLangOption(2, TagManager.GetTextGender(element), TagManager.GetTextPlural(element));
                if (IsLanguageAllowed(AllowedLanguageKey.IsGermanAllowed) || EnglishTextBox.Text != GermanTextBox.Text)
                {
                    EnableLanguage(AllowedLanguageKey.IsGermanAllowed);
                }
                else
                {
                    DisableLanguage(AllowedLanguageKey.IsGermanAllowed, 2);
                }

                element = currentTag.Element("Italian");
                ItalianTextBox.Text = TagManager.GetTextValue(element);
                UpdateLangOption(3, TagManager.GetTextGender(element), TagManager.GetTextPlural(element));
                if (IsLanguageAllowed(AllowedLanguageKey.IsItalianAllowed) || EnglishTextBox.Text != ItalianTextBox.Text)
                {
                    EnableLanguage(AllowedLanguageKey.IsItalianAllowed);
                }
                else
                {
                    DisableLanguage(AllowedLanguageKey.IsItalianAllowed, 3);
                }

                element = currentTag.Element("Spanish");
                SpanishTextBox.Text = TagManager.GetTextValue(element);
                UpdateLangOption(4, TagManager.GetTextGender(element), TagManager.GetTextPlural(element));
                if (IsLanguageAllowed(AllowedLanguageKey.IsSpanishAllowed) || EnglishTextBox.Text != SpanishTextBox.Text)
                {
                    EnableLanguage(AllowedLanguageKey.IsSpanishAllowed);
                }
                else
                {
                    DisableLanguage(AllowedLanguageKey.IsSpanishAllowed, 4);
                }
                int index = GetCurrentLanguageIndex();
                if(index >= 0)
                {
                    MaleCheckBox.Checked = MaleOptions[index];
                    FemaleCheckBox.Checked = FemaleOptions[index];
                    SingularCheckBox.Checked = SingularOptions[index];
                    PluralCheckBox.Checked = PluralOptions[index];
                }
                var elements = tagManager.CurrentXmlElement.Elements();
                
                var indexTag = elements.ToList().FindIndex(x => TagManager.GetTextValue(x.Element("Tag")) == tag);

                TagIndexTextBox.Text = string.Format("{0}/{1}", indexTag + 1, elements.Count());
            }
        }

        private void UpdateLangOption(int index, string gender, string plural)
        {
            MaleOptions[index] = gender.Contains("Male");
            FemaleOptions[index] = gender.Contains("Female");
            SingularOptions[index] = plural.Contains("0");
            PluralOptions[index] = plural.Contains("1");
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            tagManager.UpdateCurrentTagLang("English", EnglishTextBox.Text, 0, MaleOptions, FemaleOptions, SingularOptions, PluralOptions);
            tagManager.UpdateCurrentTagLang("French", FrenchTextBox.Text, 1, MaleOptions, FemaleOptions, SingularOptions, PluralOptions);
            tagManager.UpdateCurrentTagLang("German", GermanTextBox.Text, 2, MaleOptions, FemaleOptions, SingularOptions, PluralOptions);
            tagManager.UpdateCurrentTagLang("Italian", ItalianTextBox.Text, 3, MaleOptions, FemaleOptions, SingularOptions, PluralOptions);
            tagManager.UpdateCurrentTagLang("Spanish", SpanishTextBox.Text, 4, MaleOptions, FemaleOptions, SingularOptions, PluralOptions);

            UpdateLogText(String.Format("Tag updated"), TraceLevel.Info);
        }

        private void EnglishTextBox_GotFocus(object sender, EventArgs e)
        {
            LanguageTextBox_GotFocus(EnglishTextBox, 0);
        }

        private void FrenchTextBox_GotFocus(object sender, EventArgs e)
        {
            LanguageTextBox_GotFocus(FrenchTextBox, 1);
        }

        private void GermanTextBox_GotFocus(object sender, EventArgs e)
        {
            LanguageTextBox_GotFocus(GermanTextBox, 2);
        }

        private void ItalianTextBox_GotFocus(object sender, EventArgs e)
        {
            LanguageTextBox_GotFocus(ItalianTextBox, 3);
        }

        private void SpanishTextBox_GotFocus(object sender, EventArgs e)
        {
            LanguageTextBox_GotFocus(SpanishTextBox, 4);
        }

        private void EnglishTextBox_TextChanged(object sender, EventArgs e)
        {
            LanguageTextBox_TextChanged(EnglishTextBox, 0);
            if(!FrenchTextBox.Enabled)
            {
                FrenchTextBox.Text = EnglishTextBox.Text;
            }
            if (!GermanTextBox.Enabled)
            {
                GermanTextBox.Text = EnglishTextBox.Text;
            }
            if (!ItalianTextBox.Enabled)
            {
                ItalianTextBox.Text = EnglishTextBox.Text;
            }
            if (!SpanishTextBox.Enabled)
            {
                SpanishTextBox.Text = EnglishTextBox.Text;
            }
        }

        private void FrenchTextBox_TextChanged(object sender, EventArgs e)
        {
            LanguageTextBox_TextChanged(FrenchTextBox, 1);
        }

        private void GermanTextBox_TextChanged(object sender, EventArgs e)
        {
            LanguageTextBox_TextChanged(GermanTextBox, 2);
        }

        private void ItalianTextBox_TextChanged(object sender, EventArgs e)
        {
            LanguageTextBox_TextChanged(ItalianTextBox, 3);
        }

        private void SpanishTextBox_TextChanged(object sender, EventArgs e)
        {
            LanguageTextBox_TextChanged(SpanishTextBox, 4);
        }

        private void TagTextBox_TextChanged(object sender, EventArgs e)
        {
            if(IsTagUnchanged())
            {
                TagActionButton.BackgroundImage = Properties.Resources.remove;
                UpdateButton.Enabled = true;
            }
            else
            {
                TagActionButton.BackgroundImage = Properties.Resources.add;
                UpdateButton.Enabled = false;
            }
        }

        private void TagActionButton_Click(object sender, EventArgs e)
        {
            if(IsTagUnchanged())
            {
                // It means the user want to remove the tag
                XElement currentTag = tagManager.CurrentTag;
                if (currentTag != null)
                {
                    tagManager.RemoveCurrentTag();
                    UpdateCurrentTag();
                }
            }
            else
            {
                // It means the user want to add the tag
                string tagName = TagTextBox.Text;
                string englishText = EnglishTextBox.Text;
                string frenchText = FrenchTextBox.Text;
                string germanText = GermanTextBox.Text;
                string italianText = ItalianTextBox.Text;
                string spanishText = SpanishTextBox.Text;
                bool createAfterTag = AfterTagRadioButton.Checked;
                tagManager.CreateTag(tagName, englishText, frenchText, germanText, italianText, spanishText, createAfterTag, MaleOptions, FemaleOptions, SingularOptions, PluralOptions);
                UpdateCurrentTag();
            }
        }

        private bool IsTagUnchanged()
        {
            XElement currentTag = tagManager.CurrentTag;
            if(currentTag == null)
            {
                return false;
            }
            string tag = TagManager.GetTextValue(currentTag.Element("Tag"));
            return TagTextBox.Text == tag;
        }

        private void LanguageTextBox_GotFocus(TextBox textBox, int index)
        {
            if(!textBox.Focused)
            {
                textBox.Focus();
                return;
            }
            SetLabelColorFromTextBox(CurrentLanguageTextBox, Color.Black);
            CurrentLanguageTextBox = textBox;
            SetLabelColorFromTextBox(CurrentLanguageTextBox, Color.Blue);
            MainRichTextBox.Text = FromRawTestToDisplayText(textBox.Text);

            MaleCheckBox.Checked = MaleOptions[index];
            FemaleCheckBox.Checked = FemaleOptions[index];
            SingularCheckBox.Checked = SingularOptions[index];
            PluralCheckBox.Checked = PluralOptions[index];
        }

        private string FromRawTestToDisplayText(string text)
        {
            return text.Replace("[NEWLINE]", "\n");
        }

        private string FromDisplayTextToRawTest(string text)
        {
            return text.Replace("\n", "[NEWLINE]");
        }

        private void SetLabelColorFromTextBox(TextBox textBox, Color color)
        {
            Label textLabel = null;
            if(textBox == EnglishTextBox)
            {
                textLabel = EnglishLabel;
            }
            else if (textBox == FrenchTextBox)
            {
                textLabel = FrenchLabel;
            }
            else if (textBox == GermanTextBox)
            {
                textLabel = GermanLabel;
            }
            else if (textBox == ItalianTextBox)
            {
                textLabel = Italianlabel;
            }
            else if (textBox == SpanishTextBox)
            {
                textLabel = SpanishLabel;
            }

            if(textLabel != null)
            {
                textLabel.ForeColor = color;
            }
        }

        private void LanguageTextBox_TextChanged(TextBox textBox, int index)
        {
            if(FromRawTestToDisplayText(textBox.Text) != MainRichTextBox.Text && CurrentLanguageTextBox == textBox)
            {
                MainRichTextBox.Text = FromRawTestToDisplayText(textBox.Text);
                MaleCheckBox.Checked = MaleOptions[index];
                FemaleCheckBox.Checked = FemaleOptions[index];
                SingularCheckBox.Checked = SingularOptions[index];
                PluralCheckBox.Checked = PluralOptions[index];
            }
        }

        private void MainRichTextBox_TextChanged(object sender, EventArgs e)
        {
            if(CurrentLanguageTextBox != null && FromRawTestToDisplayText(CurrentLanguageTextBox.Text) != MainRichTextBox.Text)
            {
                CurrentLanguageTextBox.Text = FromDisplayTextToRawTest(MainRichTextBox.Text);
            }
        }

        private void FolderButton_Click(object sender, EventArgs e)
        {
            string newValue = FileManager.SelectFolderPath();
            if(newValue != null)
            {
                FileManager.updateFolderPath(newValue);
                UpdateFilesListBox();
            }
            TagTextBox.Focus();
        }

        private void CivXmlEditor_Shown(object sender, EventArgs e)
        {
            UpdateFilesListBox();
        }

        private void UpdateLogText(string text, TraceLevel traceLevel = TraceLevel.Off)
        {
            InfoTextBox.Text = text;
            Color textColor;
            switch (traceLevel)
            {
                case TraceLevel.Error:
                    textColor = Color.Red;
                    break;
                case TraceLevel.Warning:
                    textColor = Color.Orange;
                    break;
                case TraceLevel.Info:
                    textColor = Color.Green;
                    break;
                default:
                    textColor = Color.Black;
                    break;
            }
            InfoTextBox.ForeColor = textColor;
            TagTextBox.Focus();
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            XElement currentTag = tagManager.CurrentTag;
            if(currentTag != null)
            {
                var nextNode = currentTag.NextNode;
                if (nextNode != null)
                {
                    tagManager.CurrentTag = (XElement)nextNode;
                }
                else
                {
                    tagManager.CurrentTag = (XElement)tagManager.CurrentXmlElement.FirstNode;
                }
                UpdateCurrentTag();
            }
            TagTextBox.Focus();
        }

        private void PreviousButton_Click(object sender, EventArgs e)
        {
            XElement currentTag = tagManager.CurrentTag;
            if (currentTag != null)
            {
                var nextNode = currentTag.PreviousNode;
                if(nextNode != null)
                {
                    tagManager.CurrentTag = (XElement) nextNode;
                }
                else
                {
                    tagManager.CurrentTag = (XElement) tagManager.CurrentXmlElement.LastNode;
                }
                UpdateCurrentTag();
            }
            TagTextBox.Focus();
        }

        private void EnableLanguage(AllowedLanguageKey languageKey, bool saveData = false)
        {
            TextBox textBox = GetTextBoxFromLanguageKey(languageKey);
            Button button = GetButtonFromLanguageKey(languageKey);

            if (textBox != null && button != null)
            {
                textBox.Enabled = true;
                if(CurrentLanguageTextBox == textBox)
                {
                    SetLabelColorFromTextBox(textBox, Color.Blue);
                }
                else
                {
                    SetLabelColorFromTextBox(textBox, Color.Black);
                }
                button.BackgroundImage = Properties.Resources.delete;

                if (saveData)
                {
                    SetLanguageAllowed(languageKey, true);
                }
            }
        }

        private void DisableLanguage(AllowedLanguageKey languageKey, int index, bool saveData = false)
        {
            TextBox textBox = GetTextBoxFromLanguageKey(languageKey);
            Button button = GetButtonFromLanguageKey(languageKey);

            if (textBox != null && button != null)
            {
                textBox.Enabled = false;
                textBox.Text = EnglishTextBox.Text;
                SetLabelColorFromTextBox(textBox, Color.Gray);
                if (CurrentLanguageTextBox == textBox)
                {
                    LanguageTextBox_GotFocus(EnglishTextBox, 0);
                    MaleOptions[index] = MaleOptions[0];
                    FemaleOptions[index] = FemaleOptions[0];
                    SingularOptions[index] = SingularOptions[0];
                    PluralOptions[index] = PluralOptions[0];
                }
                button.BackgroundImage = Properties.Resources.cancel;
                if (saveData)
                {
                    SetLanguageAllowed(languageKey, false);
                }
            }
        }

        private TextBox GetTextBoxFromLanguageKey(AllowedLanguageKey languageKey)
        {
            TextBox textBox = null;
            switch (languageKey)
            {
                case AllowedLanguageKey.IsFrenchAllowed:
                    textBox = FrenchTextBox;
                    break;
                case AllowedLanguageKey.IsGermanAllowed:
                    textBox = GermanTextBox;
                    break;
                case AllowedLanguageKey.IsItalianAllowed:
                    textBox = ItalianTextBox;
                    break;
                case AllowedLanguageKey.IsSpanishAllowed:
                    textBox = SpanishTextBox;
                    break;
            }
            return textBox;
        }

        private Button GetButtonFromLanguageKey(AllowedLanguageKey languageKey)
        {
            Button button = null;
            switch (languageKey)
            {
                case AllowedLanguageKey.IsFrenchAllowed:
                    button = FrenchButton;
                    break;
                case AllowedLanguageKey.IsGermanAllowed:
                    button = GermanButton;
                    break;
                case AllowedLanguageKey.IsItalianAllowed:
                    button = ItalianButton;
                    break;
                case AllowedLanguageKey.IsSpanishAllowed:
                    button = SpanishButton;
                    break;
            }
            return button;
        }

        private bool IsLanguageAllowed(AllowedLanguageKey languageKey)
        {
            return (bool) Properties.Settings.Default[languageKey.ToString("g")];
        }

        private void SetLanguageAllowed(AllowedLanguageKey languageKey, bool bNewValue)
        {
            Properties.Settings.Default[languageKey.ToString("g")] = bNewValue;
            Properties.Settings.Default.Save();
        }

        private void FrenchButton_Click(object sender, EventArgs e)
        {
            if(FrenchTextBox.Enabled)
            {
                DisableLanguage(AllowedLanguageKey.IsFrenchAllowed, 1, true);
            }
            else
            {
                EnableLanguage(AllowedLanguageKey.IsFrenchAllowed, true);
            }
            TagTextBox.Focus();
        }

        private void GermanButton_Click(object sender, EventArgs e)
        {
            if (GermanTextBox.Enabled)
            {
                DisableLanguage(AllowedLanguageKey.IsGermanAllowed, 2, true);
            }
            else
            {
                EnableLanguage(AllowedLanguageKey.IsGermanAllowed, true);
            }
            TagTextBox.Focus();
        }

        private void ItalianButton_Click(object sender, EventArgs e)
        {
            if (ItalianTextBox.Enabled)
            {
                DisableLanguage(AllowedLanguageKey.IsItalianAllowed, 3, true);
            }
            else
            {
                EnableLanguage(AllowedLanguageKey.IsItalianAllowed, true);
            }
            TagTextBox.Focus();
        }

        private void SpanishButton_Click(object sender, EventArgs e)
        {
            if (SpanishTextBox.Enabled)
            {
                DisableLanguage(AllowedLanguageKey.IsSpanishAllowed, 4, true);
            }
            else
            {
                EnableLanguage(AllowedLanguageKey.IsSpanishAllowed, true);
            }
            TagTextBox.Focus();
        }

        private void SaveAllButton_Click(object sender, EventArgs e)
        {
            var xmlElements = tagManager.GetXmlElements();
            FileManager.SaveCurrentFile(xmlElements);
            UpdateLogText(String.Format("Files saved"), TraceLevel.Info);
            TagTextBox.Focus();
        }

        private void InsertOutsideSelection(string beforeText, string afterText)
        {
            string newText = MainRichTextBox.Text.Insert(MainRichTextBox.SelectionStart + MainRichTextBox.SelectionLength, afterText);
            newText = newText.Insert(MainRichTextBox.SelectionStart, beforeText);
            MainRichTextBox.Text = newText;
        }

        private void BoldButton_Click(object sender, EventArgs e)
        {
            InsertOutsideSelection("[BOLD]", "[/BOLD]");
        }

        private void RedButton_Click(object sender, EventArgs e)
        {
            InsertOutsideSelection("[COLOR_WARNING_TEXT]", "[COLOR_REVERT]");
        }

        private void YellowButton_Click(object sender, EventArgs e)
        {
            InsertOutsideSelection("[COLOR_UNIT_TEXT]", "[COLOR_REVERT]");
        }

        private void GreenButton_Click(object sender, EventArgs e)
        {
            InsertOutsideSelection("[COLOR_POSITIVE_TEXT]", "[COLOR_REVERT]");
        }

        private void BlueButton_Click(object sender, EventArgs e)
        {
            InsertOutsideSelection("[COLOR_HIGHLIGHT_TEXT]", "[COLOR_REVERT]");            
        }

        private void SearchTagButton_Click(object sender, EventArgs e)
        {
            string tagToFind = TagTextBox.Text;
            SearchTag(tagToFind);
            TagTextBox.Focus();
        }

        public void SearchTag(string tagToFind, bool highlight=false)
        {
            if (tagManager.FindTag(tagToFind))
            {
                InsideSearch = true;
                var currentFilename = tagManager.CurrentFileName;
                int curIndex = FilesListBox.Items.IndexOf(currentFilename);
                FilesListBox.SetSelected(curIndex, true);
                UpdateCurrentTag();
                if (highlight)
                {
                    HighlightText(keywordsTextBox.Text.Split(' '));
                }
                UpdateLogText("Tag has been found!", TraceLevel.Info);
                InsideSearch = false;
            }
            else
            {
                UpdateLogText("Tag not found!", TraceLevel.Error);
            }
        }

        private void HighlightText(string[] keywords)
        {
            foreach (string word in keywords)
            {
                int startIndex = 0;
                while (startIndex < MainRichTextBox.TextLength)
                {

                    int wordStartIndex = MainRichTextBox.Find(word, startIndex, RichTextBoxFinds.None);
                    if (wordStartIndex != -1)
                    {
                        MainRichTextBox.SelectionStart = wordStartIndex;
                        MainRichTextBox.SelectionLength = word.Length;
                        MainRichTextBox.SelectionBackColor = Color.Yellow;
                    }
                    else
                    {
                        break;
                    }
                    startIndex += wordStartIndex + word.Length;
                }
            }
        }

        private void SearchButton_Click(object sender, EventArgs e)
        {
            string[] keywords = keywordsTextBox.Text.Split(' ');
            string[] tags = tagManager.FindKeywords(keywords);
            switch(tags.Length)
            {
                case 0:
                    UpdateLogText("No match for your keywords!", TraceLevel.Error);
                    break;
                case 1:
                    SearchTag(tags[0]);
                    break;
                default:
                    UpdateLogText(string.Format("{0} tags have been found!", tags.Length), TraceLevel.Info);
                    var form = new SearchForm(this, tags);
                    form.Show(this);
                    break;
            }
        }

        private void MaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            int index = GetCurrentLanguageIndex();
            if (index != -1)
            {
                MaleOptions[index] = MaleCheckBox.Checked;
                if (index == 0)
                {
                    UpdateUnusedLanguages();
                }
            }
        }

        private void FemaleCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            int index = GetCurrentLanguageIndex();
            if (index != -1)
            {
                FemaleOptions[index] = FemaleCheckBox.Checked;
                if (index == 0)
                {
                    UpdateUnusedLanguages();
                }
            }
        }

        private void SingularCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            int index = GetCurrentLanguageIndex();
            if (index != -1)
            {
                SingularOptions[index] = SingularCheckBox.Checked;
                if (index == 0)
                {
                    UpdateUnusedLanguages();
                }
            }
        }

        private void PluralCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            int index = GetCurrentLanguageIndex();
            if (index != -1)
            {
                PluralOptions[index] = PluralCheckBox.Checked;
                if(index == 0)
                {
                    UpdateUnusedLanguages();
                }
            }
        }

        private void UpdateUnusedLanguages()
        {
            int index = 1;
            if (!FrenchTextBox.Enabled)
            {
                MaleOptions[index] = MaleOptions[0];
                FemaleOptions[index] = FemaleOptions[0];
                SingularOptions[index] = SingularOptions[0];
                PluralOptions[index] = PluralOptions[0];
            }
            index = 2;
            if (!GermanTextBox.Enabled)
            {
                MaleOptions[index] = MaleOptions[0];
                FemaleOptions[index] = FemaleOptions[0];
                SingularOptions[index] = SingularOptions[0];
                PluralOptions[index] = PluralOptions[0];
            }
            index = 3;
            if (!ItalianTextBox.Enabled)
            {
                MaleOptions[index] = MaleOptions[0];
                FemaleOptions[index] = FemaleOptions[0];
                SingularOptions[index] = SingularOptions[0];
                PluralOptions[index] = PluralOptions[0];
            }
            index = 4;
            if (!SpanishTextBox.Enabled)
            {
                MaleOptions[index] = MaleOptions[0];
                FemaleOptions[index] = FemaleOptions[0];
                SingularOptions[index] = SingularOptions[0];
                PluralOptions[index] = PluralOptions[0];
            }
        }

        private int GetCurrentLanguageIndex()
        {
            if(CurrentLanguageTextBox == EnglishTextBox)
            {
                return 0;
            }
            if (CurrentLanguageTextBox == FrenchTextBox)
            {
                return 1;
            }
            if (CurrentLanguageTextBox == GermanTextBox)
            {
                return 2;
            }
            if (CurrentLanguageTextBox == ItalianTextBox)
            {
                return 3;
            }
            if (CurrentLanguageTextBox == SpanishTextBox)
            {
                return 4;
            }
            return -1;
        }
    }
}
