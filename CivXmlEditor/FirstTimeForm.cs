﻿using CivXmlEditor.Managers;
using System;
using System.Windows.Forms;

namespace CivXmlEditor
{
    public partial class FirstTimeForm : Form
    {
        private CivXmlEditor BaseForm { get; set; }

        public FirstTimeForm(CivXmlEditor baseForm)
        {
            BaseForm = baseForm;
            InitializeComponent();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Dispose(true);
        }

        private void SelectButton_Click(object sender, EventArgs e)
        {
            string folderPath = BaseForm.FileManager.SelectFolderPath();
            BaseForm.FileManager.updateFolderPath(folderPath);
            BaseForm.UpdateFilesListBox();
            Dispose(true);
        }
    }
}
