﻿using System;
using System.Windows.Forms;

namespace CivXmlEditor
{
    partial class CivXmlEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CivXmlEditor));
            this.InfoTextBox = new System.Windows.Forms.TextBox();
            this.NextButton = new System.Windows.Forms.Button();
            this.TagIndexTextBox = new System.Windows.Forms.TextBox();
            this.PreviousButton = new System.Windows.Forms.Button();
            this.UpdateButton = new System.Windows.Forms.Button();
            this.TagActionButton = new System.Windows.Forms.Button();
            this.SearchTagButton = new System.Windows.Forms.Button();
            this.SaveAllButton = new System.Windows.Forms.Button();
            this.FolderButton = new System.Windows.Forms.Button();
            this.FilesListBox = new System.Windows.Forms.ListBox();
            this.SearchButton = new System.Windows.Forms.Button();
            this.FrenchButton = new System.Windows.Forms.Button();
            this.GermanButton = new System.Windows.Forms.Button();
            this.ItalianButton = new System.Windows.Forms.Button();
            this.SpanishButton = new System.Windows.Forms.Button();
            this.BlueButton = new System.Windows.Forms.Button();
            this.GreenButton = new System.Windows.Forms.Button();
            this.YellowButton = new System.Windows.Forms.Button();
            this.RedButton = new System.Windows.Forms.Button();
            this.BoldButton = new System.Windows.Forms.Button();
            this.MainRichTextBox = new System.Windows.Forms.RichTextBox();
            this.SpanishLabel = new System.Windows.Forms.Label();
            this.Italianlabel = new System.Windows.Forms.Label();
            this.GermanLabel = new System.Windows.Forms.Label();
            this.FrenchLabel = new System.Windows.Forms.Label();
            this.EnglishLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SpanishTextBox = new System.Windows.Forms.TextBox();
            this.ItalianTextBox = new System.Windows.Forms.TextBox();
            this.GermanTextBox = new System.Windows.Forms.TextBox();
            this.FrenchTextBox = new System.Windows.Forms.TextBox();
            this.EnglishTextBox = new System.Windows.Forms.TextBox();
            this.TagTextBox = new System.Windows.Forms.TextBox();
            this.keywordsTextBox = new System.Windows.Forms.TextBox();
            this.AfterTagRadioButton = new System.Windows.Forms.RadioButton();
            this.EndOfFileRadio = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.MaleCheckBox = new System.Windows.Forms.CheckBox();
            this.SingularCheckBox = new System.Windows.Forms.CheckBox();
            this.FemaleCheckBox = new System.Windows.Forms.CheckBox();
            this.PluralCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // InfoTextBox
            // 
            this.InfoTextBox.Font = new System.Drawing.Font("Calibri", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoTextBox.Location = new System.Drawing.Point(809, 326);
            this.InfoTextBox.Name = "InfoTextBox";
            this.InfoTextBox.Size = new System.Drawing.Size(384, 27);
            this.InfoTextBox.TabIndex = 70;
            this.InfoTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // NextButton
            // 
            this.NextButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.next;
            this.NextButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.NextButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.NextButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.NextButton.Location = new System.Drawing.Point(773, 74);
            this.NextButton.Name = "NextButton";
            this.NextButton.Size = new System.Drawing.Size(35, 35);
            this.NextButton.TabIndex = 69;
            this.NextButton.UseVisualStyleBackColor = true;
            this.NextButton.Click += new System.EventHandler(this.NextButton_Click);
            // 
            // TagIndexTextBox
            // 
            this.TagIndexTextBox.HideSelection = false;
            this.TagIndexTextBox.Location = new System.Drawing.Point(648, 75);
            this.TagIndexTextBox.Name = "TagIndexTextBox";
            this.TagIndexTextBox.ReadOnly = true;
            this.TagIndexTextBox.Size = new System.Drawing.Size(119, 26);
            this.TagIndexTextBox.TabIndex = 68;
            this.TagIndexTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // PreviousButton
            // 
            this.PreviousButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.previous;
            this.PreviousButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.PreviousButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.PreviousButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.PreviousButton.Location = new System.Drawing.Point(612, 74);
            this.PreviousButton.Name = "PreviousButton";
            this.PreviousButton.Size = new System.Drawing.Size(35, 35);
            this.PreviousButton.TabIndex = 67;
            this.PreviousButton.UseVisualStyleBackColor = true;
            this.PreviousButton.Click += new System.EventHandler(this.PreviousButton_Click);
            // 
            // UpdateButton
            // 
            this.UpdateButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.update;
            this.UpdateButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.UpdateButton.Enabled = false;
            this.UpdateButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.UpdateButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.UpdateButton.Location = new System.Drawing.Point(576, 74);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(35, 35);
            this.UpdateButton.TabIndex = 66;
            this.UpdateButton.UseVisualStyleBackColor = true;
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // TagActionButton
            // 
            this.TagActionButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.remove;
            this.TagActionButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.TagActionButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.TagActionButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TagActionButton.Location = new System.Drawing.Point(540, 74);
            this.TagActionButton.Name = "TagActionButton";
            this.TagActionButton.Size = new System.Drawing.Size(35, 35);
            this.TagActionButton.TabIndex = 65;
            this.TagActionButton.UseVisualStyleBackColor = true;
            this.TagActionButton.Click += new System.EventHandler(this.TagActionButton_Click);
            // 
            // SearchTagButton
            // 
            this.SearchTagButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.search;
            this.SearchTagButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.SearchTagButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.SearchTagButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SearchTagButton.Location = new System.Drawing.Point(504, 74);
            this.SearchTagButton.Name = "SearchTagButton";
            this.SearchTagButton.Size = new System.Drawing.Size(35, 35);
            this.SearchTagButton.TabIndex = 64;
            this.SearchTagButton.UseVisualStyleBackColor = true;
            this.SearchTagButton.Click += new System.EventHandler(this.SearchTagButton_Click);
            // 
            // SaveAllButton
            // 
            this.SaveAllButton.Image = global::CivXmlEditor.Properties.Resources.save;
            this.SaveAllButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SaveAllButton.Location = new System.Drawing.Point(939, 23);
            this.SaveAllButton.Name = "SaveAllButton";
            this.SaveAllButton.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.SaveAllButton.Size = new System.Drawing.Size(135, 35);
            this.SaveAllButton.TabIndex = 63;
            this.SaveAllButton.Text = "Save All";
            this.SaveAllButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SaveAllButton.UseVisualStyleBackColor = true;
            this.SaveAllButton.Click += new System.EventHandler(this.SaveAllButton_Click);
            // 
            // FolderButton
            // 
            this.FolderButton.Image = global::CivXmlEditor.Properties.Resources.folder;
            this.FolderButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.FolderButton.Location = new System.Drawing.Point(1080, 23);
            this.FolderButton.Name = "FolderButton";
            this.FolderButton.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.FolderButton.Size = new System.Drawing.Size(113, 35);
            this.FolderButton.TabIndex = 62;
            this.FolderButton.Text = "Folder";
            this.FolderButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.FolderButton.UseVisualStyleBackColor = true;
            this.FolderButton.Click += new System.EventHandler(this.FolderButton_Click);
            // 
            // FilesListBox
            // 
            this.FilesListBox.FormattingEnabled = true;
            this.FilesListBox.ItemHeight = 20;
            this.FilesListBox.Location = new System.Drawing.Point(809, 75);
            this.FilesListBox.Name = "FilesListBox";
            this.FilesListBox.ScrollAlwaysVisible = true;
            this.FilesListBox.Size = new System.Drawing.Size(384, 244);
            this.FilesListBox.TabIndex = 61;
            this.FilesListBox.SelectedValueChanged += new System.EventHandler(this.FilesListBox_SelectedValueChanged);
            // 
            // SearchButton
            // 
            this.SearchButton.Image = global::CivXmlEditor.Properties.Resources.search;
            this.SearchButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.SearchButton.Location = new System.Drawing.Point(665, 23);
            this.SearchButton.Name = "SearchButton";
            this.SearchButton.Padding = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.SearchButton.Size = new System.Drawing.Size(127, 35);
            this.SearchButton.TabIndex = 60;
            this.SearchButton.Text = "Search";
            this.SearchButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SearchButton.UseVisualStyleBackColor = true;
            this.SearchButton.Click += new System.EventHandler(this.SearchButton_Click);
            // 
            // FrenchButton
            // 
            this.FrenchButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.delete;
            this.FrenchButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.FrenchButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.FrenchButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.FrenchButton.Location = new System.Drawing.Point(504, 155);
            this.FrenchButton.Name = "FrenchButton";
            this.FrenchButton.Size = new System.Drawing.Size(35, 35);
            this.FrenchButton.TabIndex = 59;
            this.FrenchButton.UseVisualStyleBackColor = true;
            this.FrenchButton.Click += new System.EventHandler(this.FrenchButton_Click);
            // 
            // GermanButton
            // 
            this.GermanButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.delete;
            this.GermanButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.GermanButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.GermanButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.GermanButton.Location = new System.Drawing.Point(504, 195);
            this.GermanButton.Name = "GermanButton";
            this.GermanButton.Size = new System.Drawing.Size(35, 35);
            this.GermanButton.TabIndex = 58;
            this.GermanButton.UseVisualStyleBackColor = true;
            this.GermanButton.Click += new System.EventHandler(this.GermanButton_Click);
            // 
            // ItalianButton
            // 
            this.ItalianButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.delete;
            this.ItalianButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ItalianButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.ItalianButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ItalianButton.Location = new System.Drawing.Point(504, 235);
            this.ItalianButton.Name = "ItalianButton";
            this.ItalianButton.Size = new System.Drawing.Size(35, 35);
            this.ItalianButton.TabIndex = 57;
            this.ItalianButton.UseVisualStyleBackColor = true;
            this.ItalianButton.Click += new System.EventHandler(this.ItalianButton_Click);
            // 
            // SpanishButton
            // 
            this.SpanishButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.delete;
            this.SpanishButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.SpanishButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.SpanishButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SpanishButton.Location = new System.Drawing.Point(504, 275);
            this.SpanishButton.Name = "SpanishButton";
            this.SpanishButton.Size = new System.Drawing.Size(35, 35);
            this.SpanishButton.TabIndex = 56;
            this.SpanishButton.UseVisualStyleBackColor = true;
            this.SpanishButton.Click += new System.EventHandler(this.SpanishButton_Click);
            // 
            // BlueButton
            // 
            this.BlueButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.blue;
            this.BlueButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BlueButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.BlueButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BlueButton.Location = new System.Drawing.Point(182, 321);
            this.BlueButton.Name = "BlueButton";
            this.BlueButton.Size = new System.Drawing.Size(36, 35);
            this.BlueButton.TabIndex = 55;
            this.BlueButton.UseVisualStyleBackColor = true;
            this.BlueButton.Click += new System.EventHandler(this.BlueButton_Click);
            // 
            // GreenButton
            // 
            this.GreenButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.green;
            this.GreenButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.GreenButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.GreenButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.GreenButton.Location = new System.Drawing.Point(140, 321);
            this.GreenButton.Name = "GreenButton";
            this.GreenButton.Size = new System.Drawing.Size(36, 35);
            this.GreenButton.TabIndex = 54;
            this.GreenButton.UseVisualStyleBackColor = true;
            this.GreenButton.Click += new System.EventHandler(this.GreenButton_Click);
            // 
            // YellowButton
            // 
            this.YellowButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.yellow;
            this.YellowButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.YellowButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.YellowButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.YellowButton.Location = new System.Drawing.Point(98, 321);
            this.YellowButton.Name = "YellowButton";
            this.YellowButton.Size = new System.Drawing.Size(36, 35);
            this.YellowButton.TabIndex = 53;
            this.YellowButton.UseVisualStyleBackColor = true;
            this.YellowButton.Click += new System.EventHandler(this.YellowButton_Click);
            // 
            // RedButton
            // 
            this.RedButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.red;
            this.RedButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.RedButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.RedButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RedButton.Location = new System.Drawing.Point(56, 321);
            this.RedButton.Name = "RedButton";
            this.RedButton.Size = new System.Drawing.Size(36, 35);
            this.RedButton.TabIndex = 52;
            this.RedButton.UseVisualStyleBackColor = true;
            this.RedButton.Click += new System.EventHandler(this.RedButton_Click);
            // 
            // BoldButton
            // 
            this.BoldButton.BackgroundImage = global::CivXmlEditor.Properties.Resources.font_bold;
            this.BoldButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BoldButton.FlatAppearance.BorderColor = System.Drawing.Color.Gray;
            this.BoldButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BoldButton.Location = new System.Drawing.Point(14, 321);
            this.BoldButton.Name = "BoldButton";
            this.BoldButton.Size = new System.Drawing.Size(36, 35);
            this.BoldButton.TabIndex = 51;
            this.BoldButton.UseVisualStyleBackColor = true;
            this.BoldButton.Click += new System.EventHandler(this.BoldButton_Click);
            // 
            // MainRichTextBox
            // 
            this.MainRichTextBox.Location = new System.Drawing.Point(14, 362);
            this.MainRichTextBox.Name = "MainRichTextBox";
            this.MainRichTextBox.Size = new System.Drawing.Size(1179, 234);
            this.MainRichTextBox.TabIndex = 50;
            this.MainRichTextBox.Text = "";
            this.MainRichTextBox.TextChanged += new System.EventHandler(this.MainRichTextBox_TextChanged);
            // 
            // SpanishLabel
            // 
            this.SpanishLabel.AutoSize = true;
            this.SpanishLabel.Location = new System.Drawing.Point(10, 278);
            this.SpanishLabel.Name = "SpanishLabel";
            this.SpanishLabel.Size = new System.Drawing.Size(71, 20);
            this.SpanishLabel.TabIndex = 49;
            this.SpanishLabel.Text = "Spanish:";
            this.SpanishLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Italianlabel
            // 
            this.Italianlabel.AutoSize = true;
            this.Italianlabel.Location = new System.Drawing.Point(10, 238);
            this.Italianlabel.Name = "Italianlabel";
            this.Italianlabel.Size = new System.Drawing.Size(56, 20);
            this.Italianlabel.TabIndex = 48;
            this.Italianlabel.Text = "Italian:";
            this.Italianlabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // GermanLabel
            // 
            this.GermanLabel.AutoSize = true;
            this.GermanLabel.Location = new System.Drawing.Point(10, 198);
            this.GermanLabel.Name = "GermanLabel";
            this.GermanLabel.Size = new System.Drawing.Size(71, 20);
            this.GermanLabel.TabIndex = 47;
            this.GermanLabel.Text = "German:";
            this.GermanLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FrenchLabel
            // 
            this.FrenchLabel.AutoSize = true;
            this.FrenchLabel.Location = new System.Drawing.Point(10, 158);
            this.FrenchLabel.Name = "FrenchLabel";
            this.FrenchLabel.Size = new System.Drawing.Size(63, 20);
            this.FrenchLabel.TabIndex = 46;
            this.FrenchLabel.Text = "French:";
            this.FrenchLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // EnglishLabel
            // 
            this.EnglishLabel.AutoSize = true;
            this.EnglishLabel.Location = new System.Drawing.Point(10, 118);
            this.EnglishLabel.Name = "EnglishLabel";
            this.EnglishLabel.Size = new System.Drawing.Size(65, 20);
            this.EnglishLabel.TabIndex = 45;
            this.EnglishLabel.Text = "English:";
            this.EnglishLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(10, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 20);
            this.label2.TabIndex = 44;
            this.label2.Text = "Tag:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 20);
            this.label1.TabIndex = 43;
            this.label1.Text = "Keywords:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SpanishTextBox
            // 
            this.SpanishTextBox.Location = new System.Drawing.Point(98, 275);
            this.SpanishTextBox.Name = "SpanishTextBox";
            this.SpanishTextBox.Size = new System.Drawing.Size(400, 26);
            this.SpanishTextBox.TabIndex = 42;
            this.SpanishTextBox.TextChanged += new System.EventHandler(this.SpanishTextBox_TextChanged);
            this.SpanishTextBox.GotFocus += new System.EventHandler(this.SpanishTextBox_GotFocus);
            // 
            // ItalianTextBox
            // 
            this.ItalianTextBox.Location = new System.Drawing.Point(98, 235);
            this.ItalianTextBox.Name = "ItalianTextBox";
            this.ItalianTextBox.Size = new System.Drawing.Size(400, 26);
            this.ItalianTextBox.TabIndex = 41;
            this.ItalianTextBox.TextChanged += new System.EventHandler(this.ItalianTextBox_TextChanged);
            this.ItalianTextBox.GotFocus += new System.EventHandler(this.ItalianTextBox_GotFocus);
            // 
            // GermanTextBox
            // 
            this.GermanTextBox.Location = new System.Drawing.Point(98, 195);
            this.GermanTextBox.Name = "GermanTextBox";
            this.GermanTextBox.Size = new System.Drawing.Size(400, 26);
            this.GermanTextBox.TabIndex = 40;
            this.GermanTextBox.TextChanged += new System.EventHandler(this.GermanTextBox_TextChanged);
            this.GermanTextBox.GotFocus += new System.EventHandler(this.GermanTextBox_GotFocus);
            // 
            // FrenchTextBox
            // 
            this.FrenchTextBox.Location = new System.Drawing.Point(98, 155);
            this.FrenchTextBox.Name = "FrenchTextBox";
            this.FrenchTextBox.Size = new System.Drawing.Size(400, 26);
            this.FrenchTextBox.TabIndex = 39;
            this.FrenchTextBox.TextChanged += new System.EventHandler(this.FrenchTextBox_TextChanged);
            this.FrenchTextBox.GotFocus += new System.EventHandler(this.FrenchTextBox_GotFocus);
            // 
            // EnglishTextBox
            // 
            this.EnglishTextBox.Location = new System.Drawing.Point(98, 115);
            this.EnglishTextBox.Name = "EnglishTextBox";
            this.EnglishTextBox.Size = new System.Drawing.Size(400, 26);
            this.EnglishTextBox.TabIndex = 38;
            this.EnglishTextBox.TextChanged += new System.EventHandler(this.EnglishTextBox_TextChanged);
            this.EnglishTextBox.GotFocus += new System.EventHandler(this.EnglishTextBox_GotFocus);
            // 
            // TagTextBox
            // 
            this.TagTextBox.Location = new System.Drawing.Point(98, 75);
            this.TagTextBox.Name = "TagTextBox";
            this.TagTextBox.Size = new System.Drawing.Size(400, 26);
            this.TagTextBox.TabIndex = 37;
            this.TagTextBox.TextChanged += new System.EventHandler(this.TagTextBox_TextChanged);
            // 
            // keywordsTextBox
            // 
            this.keywordsTextBox.Location = new System.Drawing.Point(98, 25);
            this.keywordsTextBox.Name = "keywordsTextBox";
            this.keywordsTextBox.Size = new System.Drawing.Size(550, 26);
            this.keywordsTextBox.TabIndex = 36;
            // 
            // AfterTagRadioButton
            // 
            this.AfterTagRadioButton.AutoSize = true;
            this.AfterTagRadioButton.Checked = true;
            this.AfterTagRadioButton.Location = new System.Drawing.Point(560, 144);
            this.AfterTagRadioButton.Name = "AfterTagRadioButton";
            this.AfterTagRadioButton.Size = new System.Drawing.Size(126, 24);
            this.AfterTagRadioButton.TabIndex = 72;
            this.AfterTagRadioButton.TabStop = true;
            this.AfterTagRadioButton.Text = "After Current";
            this.AfterTagRadioButton.UseVisualStyleBackColor = true;
            // 
            // EndOfFileRadio
            // 
            this.EndOfFileRadio.AutoSize = true;
            this.EndOfFileRadio.Location = new System.Drawing.Point(687, 144);
            this.EndOfFileRadio.Name = "EndOfFileRadio";
            this.EndOfFileRadio.Size = new System.Drawing.Size(105, 24);
            this.EndOfFileRadio.TabIndex = 73;
            this.EndOfFileRadio.Text = "End of file";
            this.EndOfFileRadio.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(556, 118);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 20);
            this.label3.TabIndex = 74;
            this.label3.Text = "Create Tag:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MaleCheckBox
            // 
            this.MaleCheckBox.AutoSize = true;
            this.MaleCheckBox.Location = new System.Drawing.Point(293, 327);
            this.MaleCheckBox.Name = "MaleCheckBox";
            this.MaleCheckBox.Size = new System.Drawing.Size(69, 24);
            this.MaleCheckBox.TabIndex = 75;
            this.MaleCheckBox.Text = "Male";
            this.MaleCheckBox.UseVisualStyleBackColor = true;
            this.MaleCheckBox.CheckedChanged += new System.EventHandler(this.MaleCheckBox_CheckedChanged);
            // 
            // SingularCheckBox
            // 
            this.SingularCheckBox.AutoSize = true;
            this.SingularCheckBox.Location = new System.Drawing.Point(462, 327);
            this.SingularCheckBox.Name = "SingularCheckBox";
            this.SingularCheckBox.Size = new System.Drawing.Size(93, 24);
            this.SingularCheckBox.TabIndex = 76;
            this.SingularCheckBox.Text = "Singular";
            this.SingularCheckBox.UseVisualStyleBackColor = true;
            this.SingularCheckBox.CheckedChanged += new System.EventHandler(this.SingularCheckBox_CheckedChanged);
            // 
            // FemaleCheckBox
            // 
            this.FemaleCheckBox.AutoSize = true;
            this.FemaleCheckBox.Location = new System.Drawing.Point(368, 327);
            this.FemaleCheckBox.Name = "FemaleCheckBox";
            this.FemaleCheckBox.Size = new System.Drawing.Size(88, 24);
            this.FemaleCheckBox.TabIndex = 77;
            this.FemaleCheckBox.Text = "Female";
            this.FemaleCheckBox.UseVisualStyleBackColor = true;
            this.FemaleCheckBox.CheckedChanged += new System.EventHandler(this.FemaleCheckBox_CheckedChanged);
            // 
            // PluralCheckBox
            // 
            this.PluralCheckBox.AutoSize = true;
            this.PluralCheckBox.Location = new System.Drawing.Point(561, 327);
            this.PluralCheckBox.Name = "PluralCheckBox";
            this.PluralCheckBox.Size = new System.Drawing.Size(74, 24);
            this.PluralCheckBox.TabIndex = 78;
            this.PluralCheckBox.Text = "Plural";
            this.PluralCheckBox.UseVisualStyleBackColor = true;
            this.PluralCheckBox.CheckedChanged += new System.EventHandler(this.PluralCheckBox_CheckedChanged);
            // 
            // CivXmlEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1214, 614);
            this.Controls.Add(this.PluralCheckBox);
            this.Controls.Add(this.FemaleCheckBox);
            this.Controls.Add(this.SingularCheckBox);
            this.Controls.Add(this.MaleCheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.EndOfFileRadio);
            this.Controls.Add(this.AfterTagRadioButton);
            this.Controls.Add(this.InfoTextBox);
            this.Controls.Add(this.NextButton);
            this.Controls.Add(this.TagIndexTextBox);
            this.Controls.Add(this.PreviousButton);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.TagActionButton);
            this.Controls.Add(this.SearchTagButton);
            this.Controls.Add(this.SaveAllButton);
            this.Controls.Add(this.FolderButton);
            this.Controls.Add(this.FilesListBox);
            this.Controls.Add(this.SearchButton);
            this.Controls.Add(this.FrenchButton);
            this.Controls.Add(this.GermanButton);
            this.Controls.Add(this.ItalianButton);
            this.Controls.Add(this.SpanishButton);
            this.Controls.Add(this.BlueButton);
            this.Controls.Add(this.GreenButton);
            this.Controls.Add(this.YellowButton);
            this.Controls.Add(this.RedButton);
            this.Controls.Add(this.BoldButton);
            this.Controls.Add(this.MainRichTextBox);
            this.Controls.Add(this.SpanishLabel);
            this.Controls.Add(this.Italianlabel);
            this.Controls.Add(this.GermanLabel);
            this.Controls.Add(this.FrenchLabel);
            this.Controls.Add(this.EnglishLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SpanishTextBox);
            this.Controls.Add(this.ItalianTextBox);
            this.Controls.Add(this.GermanTextBox);
            this.Controls.Add(this.FrenchTextBox);
            this.Controls.Add(this.EnglishTextBox);
            this.Controls.Add(this.TagTextBox);
            this.Controls.Add(this.keywordsTextBox);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CivXmlEditor";
            this.Text = "CivXmlEditor";
            this.Shown += new System.EventHandler(this.CivXmlEditor_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox InfoTextBox;
        private System.Windows.Forms.Button NextButton;
        private System.Windows.Forms.TextBox TagIndexTextBox;
        private System.Windows.Forms.Button PreviousButton;
        private System.Windows.Forms.Button UpdateButton;
        private System.Windows.Forms.Button TagActionButton;
        private System.Windows.Forms.Button SearchTagButton;
        private System.Windows.Forms.Button SaveAllButton;
        private System.Windows.Forms.Button FolderButton;
        private System.Windows.Forms.ListBox FilesListBox;
        private System.Windows.Forms.Button SearchButton;
        private System.Windows.Forms.Button FrenchButton;
        private System.Windows.Forms.Button GermanButton;
        private System.Windows.Forms.Button ItalianButton;
        private System.Windows.Forms.Button SpanishButton;
        private System.Windows.Forms.Button BlueButton;
        private System.Windows.Forms.Button GreenButton;
        private System.Windows.Forms.Button YellowButton;
        private System.Windows.Forms.Button RedButton;
        private System.Windows.Forms.Button BoldButton;
        private System.Windows.Forms.RichTextBox MainRichTextBox;
        private System.Windows.Forms.Label SpanishLabel;
        private System.Windows.Forms.Label Italianlabel;
        private System.Windows.Forms.Label GermanLabel;
        private System.Windows.Forms.Label FrenchLabel;
        private System.Windows.Forms.Label EnglishLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SpanishTextBox;
        private System.Windows.Forms.TextBox ItalianTextBox;
        private System.Windows.Forms.TextBox GermanTextBox;
        private System.Windows.Forms.TextBox FrenchTextBox;
        private System.Windows.Forms.TextBox EnglishTextBox;
        private System.Windows.Forms.TextBox TagTextBox;
        private System.Windows.Forms.TextBox keywordsTextBox;
        private RadioButton AfterTagRadioButton;
        private RadioButton EndOfFileRadio;
        private Label label3;
        private CheckBox MaleCheckBox;
        private CheckBox SingularCheckBox;
        private CheckBox FemaleCheckBox;
        private CheckBox PluralCheckBox;
    }
}

