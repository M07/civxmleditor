﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace CivXmlEditor.Managers
{
    public class TagManager
    {
        private Dictionary<string, XElement> XmlElements { get; set; }

        public string CurrentFileName { get; set; }

        public XElement CurrentXmlElement { get; set; }

        public XElement CurrentTag { get; set; }

        public void InitXmlElements(Dictionary<string, XElement> xmlElements)
        {
            XmlElements = new Dictionary<string, XElement>();
            foreach (KeyValuePair<string, XElement> entry in xmlElements)
            {
                XmlElements[entry.Key] = RemoveAllNamespaces(entry.Value);
            }
        }

        public Dictionary<string, XElement> GetXmlElements()
        {
            XNamespace ns = "http://www.firaxis.com";
            XmlElements[CurrentFileName] = CurrentXmlElement;

            Dictionary<string, XElement> xmlElements = new Dictionary<string, XElement>();
            foreach (KeyValuePair<string, XElement> entry in XmlElements)
            {
                xmlElements[entry.Key] = AddNamespaces(entry.Value, ns);
            }

            return xmlElements;
        }

        public void UpdateCurrentTag(string fileName = null)
        {
            if (XmlElements.Count == 0)
            {
                CurrentFileName = null;
                CurrentXmlElement = null;
                CurrentTag = null;
                return;
            }

            if (fileName != null)
            {
                if (CurrentXmlElement != null)
                {
                    XmlElements[CurrentFileName] = CurrentXmlElement;
                }
                CurrentFileName = fileName;
                CurrentXmlElement = null;
                CurrentTag = null;
            }

            if (string.IsNullOrEmpty(CurrentFileName))
            {
                CurrentFileName = XmlElements.Keys.First();
            }

            CurrentXmlElement = XmlElements[CurrentFileName];

            CurrentTag = (XElement)CurrentXmlElement.FirstNode;

            CurrentTag = CurrentTag;
        }

        internal void RemoveCurrentTag()
        {
            XNode nextNode = CurrentTag.NextNode;
            CurrentXmlElement.Elements().Where(x => x == CurrentTag).Remove();
            if (nextNode != null)
            {
                CurrentTag = CurrentXmlElement.Elements().Where(x => nextNode == x).FirstOrDefault();
            }
            else
            {
                CurrentTag = (XElement)CurrentXmlElement.FirstNode;
            }
        }

        public static string GetTextValue(XElement currentTag)
        {
            if (currentTag == null)
            {
                return string.Empty;
            }
            XElement textElement = currentTag.Element("Text");

            if (textElement != null)
            {
                return textElement.Value;
            }
            return currentTag.Value;
        }

        public static string GetTextGender(XElement currentTag)
        {
            if (currentTag == null)
            {
                return string.Empty;
            }
            XElement genderElement = currentTag.Element("Gender");

            if (genderElement != null)
            {
                return genderElement.Value;
            }
            return string.Empty;
        }

        public static string GetTextPlural(XElement currentTag)
        {
            if (currentTag == null)
            {
                return string.Empty;
            }
            XElement pluralElement = currentTag.Element("Plural");

            if (pluralElement != null)
            {
                return pluralElement.Value;
            }
            return string.Empty;
        }

        public void UpdateCurrentTagLang(string langName, string newText, int index, bool[] maleOptions, bool[] femaleOptions, bool[] singularOptions, bool[] pluralOptions)
        {            
            if (CurrentTag == null)
            {
                return;
            }
            var element = CurrentTag.Element(langName);
            if(element == null)
            {
                return;
            }
            element.RemoveNodes();

            if (ShouldWriteTextInRawByIndex(index, maleOptions, femaleOptions, singularOptions, pluralOptions))
            {
                element.Value = newText;
            }
            else
            {
                element.ReplaceWith(GetComplexLang(langName, index, newText, maleOptions, femaleOptions, singularOptions, pluralOptions));
            }
        }

        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }

        public static XElement AddNamespaces(XElement origin, XNamespace ns)
        {
            if (!origin.HasElements)
            {
                XElement xElement = new XElement(ns + origin.Name.LocalName);
                xElement.Value = origin.Value;

                foreach (XAttribute attribute in origin.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(ns + origin.Name.LocalName, origin.Elements().Select(el => AddNamespaces(el, ns)));
        }

        internal void CreateTag(string tagName, string englishText, string frenchText, string germanText, string italianText, string spanishText, bool createAfterTag, bool[] maleOptions, bool[] femaleOptions, bool[] singularOptions, bool[] pluralOptions)
        {
            XElement newElement = new XElement("TEXT");
            newElement.Add(new XElement("Tag", tagName));
            if(ShouldWriteTextInRawByIndex(0, maleOptions, femaleOptions, singularOptions, pluralOptions))
            {
                newElement.Add(new XElement("English", englishText));
            }
            else
            {
                newElement.Add(GetComplexLang("English", 0, englishText, maleOptions, femaleOptions, singularOptions, pluralOptions));
            }

            if (ShouldWriteTextInRawByIndex(1, maleOptions, femaleOptions, singularOptions, pluralOptions))
            {
                newElement.Add(new XElement("French", frenchText));
            }
            else
            {
                newElement.Add(GetComplexLang("French", 1, frenchText, maleOptions, femaleOptions, singularOptions, pluralOptions));
            }

            if (ShouldWriteTextInRawByIndex(2, maleOptions, femaleOptions, singularOptions, pluralOptions))
            {
                newElement.Add(new XElement("German", germanText));
            }
            else
            {
                newElement.Add(GetComplexLang("German", 2, germanText, maleOptions, femaleOptions, singularOptions, pluralOptions));
            }

            if (ShouldWriteTextInRawByIndex(3, maleOptions, femaleOptions, singularOptions, pluralOptions))
            {
                newElement.Add(new XElement("Italian", italianText));
            }
            else
            {
                newElement.Add(GetComplexLang("Italian", 3, italianText, maleOptions, femaleOptions, singularOptions, pluralOptions));
            }

            if (ShouldWriteTextInRawByIndex(4, maleOptions, femaleOptions, singularOptions, pluralOptions))
            {
                newElement.Add(new XElement("Spanish", spanishText));
            }
            else
            {
                newElement.Add(GetComplexLang("Spanish", 4, spanishText, maleOptions, femaleOptions, singularOptions, pluralOptions));
            }
            if (CurrentTag != null)
            {
                if (createAfterTag)
                {
                    CurrentTag.AddAfterSelf(newElement);
                }
                else
                {
                    CurrentXmlElement.LastNode.AddAfterSelf(newElement);
                }
            }
            else
            {
                CurrentXmlElement.AddFirst(newElement);
            }
            CurrentTag = newElement;
        }

        private XElement GetComplexLang(string langName, int index, string text, bool[] maleOptions, bool[] femaleOptions, bool[] singularOptions, bool[] pluralOptions)
        {
            var langElement = new XElement(langName);
            langElement.Add(new XElement("Text", text));
            langElement.Add(new XElement("Gender", GetGender(index, maleOptions, femaleOptions, singularOptions, pluralOptions)));
            langElement.Add(new XElement("Plural", GetPlural(index, maleOptions, femaleOptions, singularOptions, pluralOptions)));
            return langElement;
        }
        private string GetGender(int index, bool[] maleOptions, bool[] femaleOptions, bool[] singularOptions, bool[] pluralOptions)
        {
            bool duplicates = singularOptions[index] && pluralOptions[index];
            List<string> values = new List<string>();
            if (femaleOptions[index])
            {
                values.Add("Female");
            }
            if (values.Count == 0 || maleOptions[index])
            {
                values.Add("Male");
            }
            if(duplicates)
            {
                values.AddRange(values);
            }
            return String.Join(":", values);
        }

        private string GetPlural(int index, bool[] maleOptions, bool[] femaleOptions, bool[] singularOptions, bool[] pluralOptions)
        {
            bool duplicates = femaleOptions[index] && maleOptions[index];
            List<string> values = new List<string>();
            if (singularOptions[index])
            {
                values.Add("0");
                if (duplicates)
                {
                    values.Add("0");
                }
            }
            if (values.Count == 0 || pluralOptions[index])
            {
                values.Add("1");
                if (duplicates)
                {
                    values.Add("1");
                }
            }
            
            return String.Join(":", values);
        }

        private static bool ShouldWriteTextInRawByIndex(int index, bool[] maleOptions, bool[] femaleOptions, bool[] singularOptions, bool[] pluralOptions)
        {
            return ShouldWriteTextInRaw(maleOptions[index], femaleOptions[index], singularOptions[index], pluralOptions[index]);
        }

        private static bool ShouldWriteTextInRaw(bool maleOption, bool femaleOption, bool singularOption, bool pluralOption)
        {
            return !(maleOption || femaleOption || singularOption || pluralOption);
        }

        internal bool FindTag(string tagToFind)
        {
            foreach (KeyValuePair<string, XElement> entry in XmlElements)
            {
                var xmlDocument = entry.Value;
                var searchValue = xmlDocument.Elements().Where(el => el.Element("Tag").Value == tagToFind).FirstOrDefault();
                if(searchValue != null)
                {
                    UpdateCurrentTag(entry.Key);
                    CurrentTag = searchValue;
                    return true;
                }
            }
            return false;
        }

        internal string[] FindKeywords(string[] keywords)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            keywords = keywords.Where(x => x.Length > 2).Select(x => x.ToLowerInvariant()).ToArray();
            if(keywords.Length > 0)
            {
                foreach (KeyValuePair<string, XElement> entry in XmlElements)
                {
                    var xmlDocument = entry.Value;
                    foreach(var element in xmlDocument.Elements())
                    {
                        int count = CountMatchingKeywords(element, keywords);
                        if(count > 0)
                        {
                            string tag = element.Element("Tag").Value;
                            result.Add(tag, count);
                        }
                    }
                }

            }
            return result.OrderByDescending(key => key.Value).Select(x => x.Key).ToArray();
        }

        internal int CountMatchingKeywords(XElement element, string[] keywords)
        {
            int count = 0;
            foreach (var keyword in keywords)
            {
                if(GetTextValue(element.Element("Tag")).ToLowerInvariant().Contains(keyword))
                {
                    count++;
                }
                if (GetTextValue(element.Element("English")).ToLowerInvariant().Contains(keyword))
                {
                    count++;
                }
                if (GetTextValue(element.Element("French")).ToLowerInvariant().Contains(keyword))
                {
                    count++;
                }
                if (GetTextValue(element.Element("German")).ToLowerInvariant().Contains(keyword))
                {
                    count++;
                }
                if (GetTextValue(element.Element("Italian")).ToLowerInvariant().Contains(keyword))
                {
                    count++;
                }
                if (GetTextValue(element.Element("Spanish")).ToLowerInvariant().Contains(keyword))
                {
                    count++;
                }
            }
            return count;
        }
    }
}
