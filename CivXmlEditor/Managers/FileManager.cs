﻿using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Xml;
using System.Text;

namespace CivXmlEditor.Managers
{
    public class FileManager
    {
        const string XmlFolderPath = "XmlFolderPath";

        public string[] GetLoadedXmlFilesFromFolder()
        {
            string folderPath = GetFolderPath();
            if (!string.IsNullOrEmpty(folderPath))
            {
               return Directory.GetFiles(folderPath, "*.xml")
                                         .Select(Path.GetFileName)
                                         .ToArray();
            }
            return new string[] {};
        }

        public string GetFolderPath()
        {
            string folderPath = null;
            if (!DoesSettingExist())
            {
                return null;
            }
            else
            {
                folderPath = Properties.Settings.Default[XmlFolderPath] as string;
            }
            return folderPath;
        }

        public string SelectFolderPath()
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    return fbd.SelectedPath;
                }
            }
            return null;
        }

        public void updateFolderPath(string folderPath)
        {
            if (!string.IsNullOrEmpty(folderPath))
            {
                Properties.Settings.Default[XmlFolderPath] = folderPath;
                Properties.Settings.Default.Save(); // Saves settings in application configuration file
            }
        }

        public Dictionary<string, XElement> GetXmlElements()
        {
            string folderPath = GetFolderPath();
            if(string.IsNullOrEmpty(folderPath))
            {
                return null;
            }
            return Directory.GetFiles(folderPath, "*.xml").ToDictionary(Path.GetFileName, XElement.Load);
        }

        private bool DoesSettingExist()
        {
            return Properties.Settings.Default[XmlFolderPath] as string != string.Empty;
        }

        internal void SaveCurrentFile(Dictionary<string, XElement> xmlElements)
        {
            var encoding = Encoding.ASCII;
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = false,
                Encoding = encoding
            };
            foreach (KeyValuePair<string, XElement> entry in xmlElements)
            {
                using (var writer = XmlWriter.Create(GetFolderPath() + "/" + entry.Key, xmlWriterSettings))
                {
                    entry.Value.Save(writer);
                }
            }
        }
           
    }
}
